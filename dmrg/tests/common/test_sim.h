/*****************************************************************************
 *
 * ALPS MPS DMRG Project
 *
 * Copyright (C) 2014 Institute for Theoretical Physics, ETH Zurich
 *               2011-2013 by Bela Bauer <bauerb@phys.ethz.ch>
 *                            Michele Dolfi <dolfim@phys.ethz.ch>
 *
 * This software is part of the ALPS Applications, published under the ALPS
 * Application License; you can use, redistribute it and/or modify it under
 * the terms of the license, either version 1 or (at your option) any later
 * version.
 *
 * You should have received a copy of the ALPS Application License along with
 * the ALPS Applications; see the file LICENSE.txt. If not, the license is also
 * available from http://alps.comp-phys.org/.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 *****************************************************************************/

#ifndef APP_DMRG_SIM_H
#define APP_DMRG_SIM_H

#include <cmath>
#include <iterator>
#include <iostream>
#include <sys/stat.h>

#include <boost/shared_ptr.hpp>

#include "dmrg/sim/sim.h"
#include "dmrg/optimize/optimize.h"


template <class Matrix, class SymmGroup>
class test_sim : public sim<Matrix, SymmGroup> {
    // Types definition
    typedef sim<Matrix, SymmGroup> base;
    typedef optimizer_base<Matrix, SymmGroup, storage::disk> opt_base_t;
    typedef typename base::measurements_type measurements_type;
    // Inheritance of the attributes
    using base::mps_guess;
    using base::mps_sa;
    using base::mps_partial_overlap;
    using base::mpo;
    using base::mpo_squared;
    using base::parms;
    using base::stop_callback;
    using base::init_sweep;
    using base::init_site;
    using base::n_states;


public:

    test_sim (DmrgParameters & parms_) :
    base(parms_, parms_["n_states_sa"])
    {
        // Put here initialisation specific only for DMRG optimisations
        // and are not important e.g. for measurements
        // (warning, other sweep apps might need them too...)

        // Construct mps_guess for sa_alg == -3
        if (parms.is_set("sa_algorithm"))
        {
            int sa_alg = parms["sa_algorithm"] ;
            if (sa_alg == -3) {
                int ms_site = parms["lrparam_site"];
                MPS<Matrix, SymmGroup> tst_mps = mps_sa[0] ;
                int init_site_loc = init_site == -1 ? 0 : init_site ;
                if (ms_site > 0) init_site_loc = ms_site;

                mps_guess.push_back(mps_sa[0][init_site_loc]) ;
                for (std::size_t idx = 1; idx < mps_sa.size(); idx++) {
                    tst_mps += mps_sa[idx] ;
                    mps_guess.push_back(mps_sa[idx][init_site_loc]) ;
                }

                if (maquis::checks::sa_check(mps_sa, ms_site))
                {
                    maquis::cout << "Detected multi-state MPS with common boundaries at site " << ms_site << std::endl;
                }
                else
                {
                    tst_mps /= n_states ;
                    std::fill(mps_sa.begin(), mps_sa.end(), tst_mps) ;
                }
            }
        }
    }

    void run()
    {

        //
        // Optimizer initialization
        // ------------------------
        boost::shared_ptr<opt_base_t> optimizer;
        if (parms["optimization"] == "singlesite") {
            optimizer.reset( new ss_optimize<Matrix, SymmGroup, storage::disk>
                            (mps_sa, mpo, parms, stop_callback, init_site, mps_guess, mps_partial_overlap, mpo_squared) );
        } else if(parms["optimization"] == "twosite") {
            optimizer.reset( new ts_optimize<Matrix, SymmGroup, storage::disk>
                            (mps_sa, mpo, parms, stop_callback, init_site, mps_guess, mps_partial_overlap, mpo_squared) );
        } else {
            throw std::runtime_error("Don't know this optimizer");
        }

        //
        // DMRG Sweep optimization
        // -----------------------
        for (int sweep=init_sweep; sweep < parms["nsweeps"]; ++sweep)
        {
            optimizer->sweep(sweep, Both);
        }

    }

    std::vector<typename Matrix::value_type> energy()
    {
        std::vector<typename Matrix::value_type> ret;
        ret.reserve(mps_sa.size());
        for (auto&& mps: mps_sa)
            ret.push_back(expval(mps,mpo) + mpo.getCoreEnergy());

        return ret;
    }
    // ~dmrg_sim()
    // {
    //     storage::disk::sync();
    // }


};

#endif
