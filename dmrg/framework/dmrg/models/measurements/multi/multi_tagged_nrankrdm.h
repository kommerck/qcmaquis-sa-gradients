/*****************************************************************************
*
* ALPS MPS DMRG Project
*
* Copyright (C) 2014 Institute for Theoretical Physics, ETH Zurich
*               2011-2011 by Bela Bauer <bauerb@phys.ethz.ch>
*               2011-2013    Michele Dolfi <dolfim@phys.ethz.ch>
*               2014-2014    Sebastian Keller <sebkelle@phys.ethz.ch>
*               2019         Leon Freitag <lefreita@ethz.ch>
*
* This software is part of the ALPS Applications, published under the ALPS
* Application License; you can use, redistribute it and/or modify it under
* the terms of the license, either version 1 or (at your option) any later
* version.
*
* You should have received a copy of the ALPS Application License along with
* the ALPS Applications; see the file LICENSE.txt. If not, the license is also
* available from http://alps.comp-phys.org/.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
* SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
* FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*
*****************************************************************************/
#ifndef MULTI_TAGGED_NRANKRDM_H
#define MULTI_TAGGED_NRANKRDM_H

#include "dmrg/models/measurements/multi/multi_measurement.h"
#include "dmrg/models/measurements/tagged_nrankrdm.h"

// MultiTaggedNRankRDM -- class derived from TaggedNRankRDM for multistate-MPS measurements
// reusing a single set of boundaries.
namespace measurements
    {
        template <class Matrix, class SymmGroup>
        class MultiTaggedNRankRDM : public TaggedNRankRDM<Matrix, SymmGroup>, public MultiMeasurement<Matrix, SymmGroup>
        {
            protected:
                typedef std::vector<MPS<Matrix, SymmGroup> > mps_type;
                typedef measurements_details::evaluator<mps_type> evaluator_type;
                typedef typename measurements_details::evaluator<mps_type>::result_type result_type;
            public:
                typedef measurement<Matrix, SymmGroup> basebase;
                typedef TaggedNRankRDM<Matrix, SymmGroup> base;
                typedef MultiMeasurement<Matrix, SymmGroup> multi_base;
                typedef typename base::positions_type positions_type;
                typedef typename base::tag_vec tag_vec;
                typedef typename base::scaled_bond_term scaled_bond_term;
                typedef typename multi_base::states_to_measure_type states_to_measure_type;

                using multi_base::multi_base;
                using multi_base::states_to_measure;
                using multi_base::multi_results;

                using base::labels;
                using base::name;
                using base::do_measure_1rdm;
                using base::do_measure_2rdm;
                // Specialization to call the correct constructor of the base class
                // TODO: Check if this compiles with disabled SU2U1/SU2U1PG!
                // symm_traits::HasSU2<SU2U1> and symm_traits::HasSU2<SU2U1PG> yields boost::true_type
                MultiTaggedNRankRDM(boost::true_type, int expval_site_,
                        const std::string & name_, const Lattice & lat,
                        boost::shared_ptr<TagHandler<Matrix, SymmGroup> > tag_handler_,
                        typename TermMakerSU2<Matrix, SymmGroup>::OperatorCollection const & op_collection_,
                        positions_type const& positions_ = positions_type(), const states_to_measure_type & states_to_measure_ = {{0,0}})
                            : basebase(name_), // required due to virtual inheritance
                              multi_base(states_to_measure_),
                              base(name_, lat, tag_handler_, op_collection_, positions_, std::string("")),
                              expval_site(expval_site_) {}


                // 2U1 and other symmetry groups
                MultiTaggedNRankRDM(boost::false_type, int expval_site_,
                        const std::string & name_, const Lattice & lat,
                        boost::shared_ptr<TagHandler<Matrix, SymmGroup> > tag_handler_,
                        tag_vec const & identities_, tag_vec const & fillings_, std::vector<scaled_bond_term> const& ops_,
                        bool half_only_, positions_type const& positions_ = positions_type(), const states_to_measure_type states_to_measure_ = {{0,0}})
                            : multi_base(states_to_measure_, name_),
                              base(name_, lat, tag_handler_, identities_, fillings_, ops_, half_only_, positions_, std::string("")),
                              expval_site(expval_site_) { throw std::runtime_error("Multi_measure in symmetry other than SU2U1 not implemented!"); }


                // for backwards compatibility only, as usually there's no need to load the bra checkpoint from file for multiple measurements
                virtual void evaluate(MPS<Matrix, SymmGroup> const& ket_mps, boost::optional<reduced_mps<Matrix, SymmGroup> const&> rmps = boost::none)
                {
                    throw std::runtime_error("evaluate() should not be called in a measurement with multiple states");
                }

                virtual void multi_evaluate(const mps_type& mps_vec)
                {
                    this->multi_results.clear();
                    this->labels.clear();

                    evaluator_type ev(expval_site, states_to_measure);
                    bool transition_meas = false;
                    if ((this->name() == "transition_oneptdm") || (this->name() == "transition_twoptdm"))
                        transition_meas = true;
                    if (this->name() == "oneptdm" || this->name() == "transition_oneptdm")
                        std::tie(labels, multi_results) = this->do_measure_1rdm(mps_vec, mps_vec, transition_meas, ev);
                    else if (this->name() == "twoptdm" || this->name() == "transition_twoptdm")
                        std::tie(labels, multi_results) = this->do_measure_2rdm(mps_vec, mps_vec, transition_meas, ev);
                }

            protected:
                int expval_site;

                measurement<Matrix, SymmGroup>* do_clone() const
                {
                    // return new std::remove_reference<decltype(*this)>::type(*this);
                    return new MultiTaggedNRankRDM(*this);
                }

        };
    }


#endif