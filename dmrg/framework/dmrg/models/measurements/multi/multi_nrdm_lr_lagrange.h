/*****************************************************************************
*
* ALPS MPS DMRG Project
*
* Copyright (C) 2014 Institute for Theoretical Physics, ETH Zurich
*               2011-2011 by Bela Bauer <bauerb@phys.ethz.ch>
*               2011-2013    Michele Dolfi <dolfim@phys.ethz.ch>
*               2014-2014    Sebastian Keller <sebkelle@phys.ethz.ch>
*               2020         Leon Freitag <lefreita@ethz.ch>
*
* This software is part of the ALPS Applications, published under the ALPS
* Application License; you can use, redistribute it and/or modify it under
* the terms of the license, either version 1 or (at your option) any later
* version.
*
* You should have received a copy of the ALPS Application License along with
* the ALPS Applications; see the file LICENSE.txt. If not, the license is also
* available from http://alps.comp-phys.org/.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
* SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
* FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*
*****************************************************************************/
#ifndef MULTI_NRDM_LR_LAGRANGE_H
#define MULTI_NRDM_LR_LAGRANGE_H

#include "dmrg/models/measurements/multi/multi_tagged_nrankrdm.h"
#include "dmrg/models/measurements/nrdm_lr_lagrange.h"

// Class for 2-TDM measurements with externally supplied MPS tensors from text files,
// required for SA gradients
// We do not derive from NRDMLRLagrange, since it makes things more complicated

namespace measurements
{
    template <class Matrix, class SymmGroup>
        class MultiNRDMLRLagrange : public virtual MultiTaggedNRankRDM<Matrix, SymmGroup> // public NRDMLRLagrange<Matrix, SymmGroup>
        {
            protected:
                std::vector<std::string> filenames_;

            public:
                // base classes
                typedef measurement<Matrix, SymmGroup> base_measurement;
                typedef MultiMeasurement<Matrix, SymmGroup> base_MultiMeasurement;
                typedef TaggedNRankRDM<Matrix, SymmGroup> base_TaggedNRankRDM;
                typedef MultiTaggedNRankRDM<Matrix, SymmGroup> base_MultiTaggedNRankRDM;
                typedef typename base_TaggedNRankRDM::positions_type positions_type;

                typedef typename base_MultiTaggedNRankRDM::mps_type mps_type;
                typedef typename base_MultiTaggedNRankRDM::evaluator_type evaluator_type;
                typedef typename base_MultiTaggedNRankRDM::result_type result_type;

                typedef typename base_MultiMeasurement::states_to_measure_type states_to_measure_type;

                using base_measurement::labels;
                using base_measurement::name;

                using base_MultiMeasurement::multi_results;
                using base_MultiTaggedNRankRDM::expval_site;
                using base_MultiMeasurement::states_to_measure;

                using base_TaggedNRankRDM::do_measure_1rdm;
                using base_TaggedNRankRDM::do_measure_2rdm;

                MultiNRDMLRLagrange(int lr_site,
                                    const std::vector<std::string> & filenames,
                                    const std::string & name_,
                                    const Lattice & lat,
                                    boost::shared_ptr<TagHandler<Matrix, SymmGroup> > tag_handler_,
                                    typename TermMakerSU2<Matrix, SymmGroup>::OperatorCollection const & op_collection_,
                                    positions_type const& positions_ = positions_type())
                                    : base_measurement(name_),
                                    //   base_TaggedNRankRDM(name_, lat, tag_handler_, op_collection_, positions_, std::string("")),
                                      base_MultiTaggedNRankRDM(boost::true_type(), lr_site, name_, lat, tag_handler_, op_collection_, positions_),
                                      filenames_(filenames)
                                      {};



                // for backwards compatibility only, as usually there's no need to load the bra checkpoint from file for multiple measurements
                virtual void evaluate(MPS<Matrix, SymmGroup> const& ket_mps, boost::optional<reduced_mps<Matrix, SymmGroup> const&> rmps = boost::none)
                {
                    throw std::runtime_error("evaluate() should not be called in a measurement with multiple states");
                }

                virtual void multi_evaluate(const mps_type& mps_vec)
                {
                    this->multi_results.clear();
                    this->labels.clear();

                    // Overwrite states_to_measure in base_MultiMeasurement, since we're going to measure one TDM for each state
                    // so fill states_to_measure accordingly
                    states_to_measure.resize(mps_vec.size());

                    for (int i = 0; i < mps_vec.size(); i++)
                        states_to_measure[i] = std::make_pair(i,i);

                    evaluator_type ev(expval_site, states_to_measure);

                    const mps_type & aux_mps_vec = prepare_aux_mps(mps_vec);

                    if ((this->name() == "onerdmlagrangeL") || (this->name() == "onerdmlagrange"))
                        std::tie(labels, multi_results) = this->do_measure_1rdm(mps_vec, aux_mps_vec, true, ev);
                    if ((this->name() == "twordmlagrangeL") || (this->name() == "twordmlagrange"))
                        std::tie(labels, multi_results) = this->do_measure_2rdm(mps_vec, aux_mps_vec, true, ev);
                }

            private:
                // prepare auxiliary MPS from the filenames
                mps_type prepare_aux_mps(const mps_type& mps_vec)
                {
                    typedef typename Matrix::value_type value_type;

                    // only one-site MPS parameters are allowed!!
                    mps_type aux_mps_vec = mps_vec;

                    assert(mps_vec.size() == filenames_.size());
                    for (int i = 0; i < mps_vec.size(); i++)
                    {
                        std::vector<value_type> aux_elements;
                        maquis::cout << "Reading the auxiliary MPSTensor elements for state " << i << " and site "
                                     << expval_site << " from file " << filenames_[i] << std::endl;

                         // read and parse the file
                        std::ifstream infile(filenames_[i]);
                        if (infile)
                            std::copy(std::istream_iterator<value_type>(infile), std::istream_iterator<value_type>(), std::back_inserter(aux_elements));
                        else
                            throw std::runtime_error("File " + filenames_[i] + " could not be opened!");


                        MPSTensor<Matrix, SymmGroup> & mpst = aux_mps_vec[i][expval_site];
                        mpst.make_left_paired();
                        assert(mpst.data().num_elements() == aux_elements.size());
                        size_t fileidx = 0;
                        for (size_t i = 0; i < mpst.data().n_blocks(); i++)
                        for (size_t j = 0; j < mpst.data()[i].num_rows(); j++)
                        for (size_t k = 0; k < mpst.data()[i].num_cols(); k++)
                        {
                            parallel::guard::serial guard;
                            mpst.data()[i](j,k) = aux_elements[fileidx++];
                        }
                    }

                    return aux_mps_vec;
                }

        };
}

#endif