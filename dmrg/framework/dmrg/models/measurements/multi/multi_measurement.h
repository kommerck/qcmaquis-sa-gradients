/*****************************************************************************
*
* ALPS MPS DMRG Project
*
* Copyright (C) 2014 Institute for Theoretical Physics, ETH Zurich
*               2011-2011 by Bela Bauer <bauerb@phys.ethz.ch>
*               2011-2013    Michele Dolfi <dolfim@phys.ethz.ch>
*               2014-2014    Sebastian Keller <sebkelle@phys.ethz.ch>
*               2019         Leon Freitag <lefreita@ethz.ch>
*
* This software is part of the ALPS Applications, published under the ALPS
* Application License; you can use, redistribute it and/or modify it under
* the terms of the license, either version 1 or (at your option) any later
* version.
*
* You should have received a copy of the ALPS Application License along with
* the ALPS Applications; see the file LICENSE.txt. If not, the license is also
* available from http://alps.comp-phys.org/.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
* SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
* FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*
*****************************************************************************/
#ifndef MULTI_MEASUREMENT_H
#define MULTI_MEASUREMENT_H

#include "dmrg/models/measurement.h"
#include "dmrg/models/measurements/measurements_detail.h"

// Measurement class for measurements with multiple MPS (and the same boundary)

template<class Matrix, class SymmGroup>
class MultiMeasurement : public virtual measurement<Matrix, SymmGroup> {
    public:
        typedef measurement<Matrix, SymmGroup> base;
        typedef measurements_details::states_to_measure_type states_to_measure_type;

        virtual ~MultiMeasurement() {}

        MultiMeasurement(const states_to_measure_type& states_to_measure_)
             : states_to_measure(states_to_measure_) {}


        void multi_evaluate_and_save(
            const std::vector<MPS<Matrix, SymmGroup> > & mps_vector, const std::string & archive_path,
            const std::vector<std::string> & result_names) // For performance reasons, archive is not templatised
        {

            maquis::cout << "Measuring " << this->name() << std::endl;
            // Evaluate all measurements
            multi_evaluate(mps_vector);

            // Check if every measurement has a result
            assert(multi_results.size() > 0);
            assert(multi_results[0].size() == states_to_measure.size());
            // Save to archives
            for(int i = 0; i < states_to_measure.size(); i++)
            {
                // hack: abuse vector_results as a temporary to store results from multiple states
                // This is because operator<< reads vector_results;
                this->vector_results.clear();
                this->vector_results.reserve(multi_results.size());

                for (auto&& result: multi_results)
                    this->vector_results.push_back(result[i]);

                // save the result into the result file of the 1st state in the state pair
                storage::archive ar(result_names[states_to_measure[i].first], "w");

                ar[archive_path] << (*this);
            }

        }

    protected:
        // Results vector
        std::vector<std::vector<typename MPS<Matrix, SymmGroup>::scalar_type> > multi_results;

        // List of state indices to perform measurement
        // Every entry should contain a pair of indices that correspond to states in the MPS vector
        // For non-transition density measurements, both indices should be equal.
        states_to_measure_type states_to_measure;

        virtual void multi_evaluate(const std::vector<MPS<Matrix, SymmGroup> >& mps_vec)=0;
};

#endif