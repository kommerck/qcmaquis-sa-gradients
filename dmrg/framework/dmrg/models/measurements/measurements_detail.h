/*****************************************************************************
*
* ALPS MPS DMRG Project
*
* Copyright (C) 2014 Institute for Theoretical Physics, ETH Zurich
*               2011-2011 by Bela Bauer <bauerb@phys.ethz.ch>
*               2011-2013    Michele Dolfi <dolfim@phys.ethz.ch>
*               2014-2014    Sebastian Keller <sebkelle@phys.ethz.ch>
*               2019         Leon Freitag <lefreita@ethz.ch>
*
* This software is part of the ALPS Applications, published under the ALPS
* Application License; you can use, redistribute it and/or modify it under
* the terms of the license, either version 1 or (at your option) any later
* version.
*
* You should have received a copy of the ALPS Application License along with
* the ALPS Applications; see the file LICENSE.txt. If not, the license is also
* available from http://alps.comp-phys.org/.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
* SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
* FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*
*****************************************************************************/
#ifndef MEASUREMENTS_DETAIL_H
#define MEASUREMENTS_DETAIL_H

#include "dmrg/mp_tensors/boundary.h"
#include "dmrg/mp_tensors/mps_mpo_ops.h"

namespace measurements_details
{
    typedef std::vector<std::pair<std::size_t, std::size_t> > states_to_measure_type;

    // Wrapper for the expval function, depending on whether we evaluate for a single MPS or for multiple MPS with the same boundaries
    template<class T>
    class evaluator {};

    // specialization for a single MPS
    template<class Matrix, class SymmGroup>
    class evaluator<MPS<Matrix, SymmGroup> >
    {
        public:
            typedef typename MPS<Matrix, SymmGroup>::scalar_type result_type;

            template<class T>
            void init_pairing(const T& bra, const T& ket) const {};

            result_type exp_value(const MPS<Matrix, SymmGroup>& bra, const MPS<Matrix, SymmGroup>& ket, const MPO<Matrix, SymmGroup>& mpo) const
            {
                return expval(bra, ket, mpo);
            };
    };

    // specialization for multiple MPS (with the same boundaries)
    template<class Matrix, class SymmGroup>
    class evaluator<std::vector<MPS<Matrix, SymmGroup> > >
    {
        public:
            typedef std::vector<typename MPS<Matrix, SymmGroup>::scalar_type> result_type;

            evaluator<std::vector<MPS<Matrix, SymmGroup> > >(int site_, const states_to_measure_type & states_to_measure_) :
                site(site_), states_to_measure(states_to_measure_) {};

            // Pairing must be initialized before the parallel evaluation starts
            // warning -- the const-ness of the parameters is fake here as apparently one can modify the pairing of a const MPS
            void init_pairing(const std::vector<MPS<Matrix, SymmGroup> >& dummy, const std::vector<MPS<Matrix, SymmGroup> >& mps_vec) const
            {
                for (auto&& mps: mps_vec)
                {
                    assert(site < mps.length());
                    for (int i = 0; i <= site; i++)
                        mps[i].make_left_paired();
                    for (int i = site+1; i < mps.length(); i++)
                        mps[i].make_right_paired();
                }
            }

            result_type exp_value(const std::vector<MPS<Matrix, SymmGroup> >& bra_mps_vec, const std::vector<MPS<Matrix, SymmGroup> >& mps_vec, const MPO<Matrix, SymmGroup>& mpo) const
            {

                // prepare boundaries
                assert(!mps_vec.empty());
                std::pair<Boundary<Matrix, SymmGroup>, Boundary<Matrix, SymmGroup> > boundaries = prepare_boundaries(mps_vec[0], mps_vec[0], mpo, site);

                // evaluate for all MPS

                std::vector<typename MPS<Matrix, SymmGroup>::scalar_type> ret;

                ret.reserve(states_to_measure.size());
                for (auto&& state_meas : states_to_measure)
                {
                    std::size_t i = state_meas.first, j = state_meas.second;
                    assert(i < mps_vec.size() && j < mps_vec.size());
                    ret.push_back(expval_boundaries(bra_mps_vec[i], mps_vec[j], mpo, boundaries.first, boundaries.second, site));
                }

                return ret;
            }

        private:
            int site;
            const states_to_measure_type & states_to_measure;


    };
}
#endif