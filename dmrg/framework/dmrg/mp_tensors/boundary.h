/*****************************************************************************
 *
 * ALPS MPS DMRG Project
 *
 * Copyright (C) 2014 Institute for Theoretical Physics, ETH Zurich
 *               2011-2011 by Bela Bauer <bauerb@phys.ethz.ch>
 *               2017 by Alberto Baiardi <alberto.baiardi@sns.it>
 *
 * This software is part of the ALPS Applications, published under the ALPS
 * Application License; you can use, redistribute it and/or modify it under
 * the terms of the license, either version 1 or (at your option) any later
 * version.
 *
 * You should have received a copy of the ALPS Application License along with
 * the ALPS Applications; see the file LICENSE.txt. If not, the license is also
 * available from http://alps.comp-phys.org/.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 *****************************************************************************/

#ifndef BOUNDARY_H
#define BOUNDARY_H

#include "dmrg/utils/storage.h"
#include "dmrg/block_matrix/block_matrix.h"
#include "dmrg/block_matrix/indexing.h"
#include "utils/function_objects.h"
#include "dmrg/utils/parallel.hpp"

#include <iostream>
#include <set>

//
// BOUNDARY CLASS
// --------------
//
// Attributes:
// data_ : vector of block_matrices
//
// Constructors:
// Two constructors are available, one by using a second Boundary object,
// the other one giving the size and the basis
//
// Methods:
// aux_dim --> gives the auxiliary dimension of the boundary
// resize  --> change the size of the boundary (i.e. the auxiliary dimension)
// traces  --> compute the traces of each matrix
//
// Operator:
// [i]     ---> get the i-th block_matrix of the boundary
//

template<class Matrix, class SymmGroup>
class Boundary : public storage::disk::serializable<Boundary<Matrix, SymmGroup> >
{
public:
    typedef typename maquis::traits::scalar_type<Matrix>::type scalar_type;
    typedef typename Matrix::value_type value_type;
    typedef std::pair<typename SymmGroup::charge, std::size_t> access_type;

    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & data_;
    }
    //
    // Constructors
    // ------------
    Boundary(Index<SymmGroup> const & ud = Index<SymmGroup>(),
             Index<SymmGroup> const & ld = Index<SymmGroup>(),
             std::size_t ad = 1)
    : data_(ad, block_matrix<Matrix, SymmGroup>(ud, ld))
    { }

    template <class OtherMatrix>
    Boundary(Boundary<OtherMatrix, SymmGroup> const& rhs)
    {
        data_.reserve(rhs.aux_dim());
        for (std::size_t n=0; n<rhs.aux_dim(); ++n)
            data_.push_back(rhs[n]);
    }
    //
    // Methods
    // -------
    std::size_t aux_dim() const {
        return data_.size();
    }

    void resize(size_t n){
        if(n < data_.size())
            return data_.resize(n);
        data_.reserve(n);
        for(int i = data_.size(); i < n; ++i)
            data_.push_back(block_matrix<Matrix, SymmGroup>());
    }

    std::vector<scalar_type> traces() const {
        std::vector<scalar_type> ret; ret.reserve(data_.size());
        for (size_t k=0; k < data_.size(); ++k) ret.push_back(data_[k].trace());
        return ret;
    }

    bool reasonable() const {
        for(size_t i = 0; i < data_.size(); ++i)
            if(!data_[i].reasonable()) return false;
        return true;
    }

    template<class Archive>
    void load(Archive & ar){
        std::vector<std::string> children = ar.list_children("data");
        data_.resize(children.size());
        parallel::scheduler_balanced scheduler(children.size());
        for(size_t i = 0; i < children.size(); ++i){
             parallel::guard proc(scheduler(i));
             ar["data/"+children[i]] >> data_[alps::cast<std::size_t>(children[i])];
        }
    }

    template<class Archive>
    void save(Archive & ar) const {
        ar["data"] << data_;
    }

    //
    //
    Boundary const & operator+=(Boundary const & rhs)
    {
        assert (this->data_.size() == rhs.data_.size()) ;
        for(size_t i = 0; i < this->data_.size(); ++i)
            this->data_[i] += rhs.data_[i] ;
    };
    //
    Boundary const & operator-=(Boundary const & rhs)
    {
        assert (this->data_.size() == rhs.data_.size()) ;
        for(size_t i = 0; i < this->data_.size(); ++i)
            this->data_[i] -= rhs.data_[i] ;
    };
    //
    Boundary const & operator*=(scalar_type const & rhs)
    {
        for(size_t i = 0; i < this->data_.size(); ++i)
            this->data_[i] *= rhs ;
    };
    //
    Boundary const & operator/=(scalar_type const & rhs)
    {
        for(size_t i = 0; i < this->data_.size(); ++i)
            this->data_[i] /= rhs ;
    };
    //
    friend Boundary operator*(scalar_type const & rhs, const Boundary& b_rhs)
    {
        Boundary res(b_rhs);
        for(size_t i = 0; i < res.data_.size(); ++i)
            res.data_[i] *= rhs ;
        return res;
    };
    //
    friend Boundary operator/(scalar_type const & rhs, const Boundary& b_rhs)
    {
      Boundary res(b_rhs);
      for(size_t i = 0; i < res.data_.size(); ++i)
            res.data_[i] /= rhs ;
      return res;
    };

    void print()
    {
      for (auto x : data_)
        std::cout << x << std::endl;
    };
    //
    block_matrix<Matrix, SymmGroup> & operator[](std::size_t k) { return data_[k]; }
    block_matrix<Matrix, SymmGroup> const & operator[](std::size_t k) const { return data_[k]; }
    //value_type & operator()(std::size_t i, access_type j, access_type k) { return data_[i](j, k); } // I hope this is never used (30.04.2012 / scalar/value discussion)
    //value_type const & operator()(std::size_t i, access_type j, access_type k) const { return data_[i](j, k); }
private:
    std::vector<block_matrix<Matrix, SymmGroup> > data_;
};

//
//
// ADDITIONAL FUNCTIONS
// --------------------
//
// Simplify: performs an SVD of each block_matrix of the boundary and returns the
//           a "smaller" block_matrix

template<class Matrix, class SymmGroup>
Boundary<Matrix, SymmGroup> simplify(Boundary<Matrix, SymmGroup> b)
{
    typedef typename alps::numeric::associated_real_diagonal_matrix<Matrix>::type dmt;

    for (std::size_t k = 0; k < b.aux_dim(); ++k)
    {
        block_matrix<Matrix, SymmGroup> U, V, t;
        block_matrix<dmt, SymmGroup> S;

        if (b[k].basis().sum_of_left_sizes() == 0)
            continue;

        svd_truncate(b[k], U, V, S, 1e-4, 1, false);

        gemm(U, S, t);
        gemm(t, V, b[k]);
    }

    return b;
}

template<class Matrix, class SymmGroup>
std::size_t size_of(Boundary<Matrix, SymmGroup> const & m)
{
    size_t r = 0;
    for (size_t i = 0; i < m.aux_dim(); ++i)
        r += size_of(m[i]);
    return r;
}

/*
// Not working yet and therefore commented.
// Right now one can take a product of two boundaries left[l] and right[l] using
// ietl::dot(bra[l], site_hamil2(ket[l],left[l-1], right[l], mpo[l])) [mind the correct indices]
// as is done in the new expval (see mps_mpo_ops.h).
//
// Scalar product of two boundaries
//  __         b_l      b_l
//  >__       L        R
//b_la_la'_l   a'_la_l  a_la'_l
// the MPO is needed to exploit the hermiticity of the boundaries (otherwise it doesn't work!)
template<class Matrix, class SymmGroup>
typename Boundary<Matrix, SymmGroup>::scalar_type
 product(const Boundary<Matrix, SymmGroup>& left, const Boundary<Matrix, SymmGroup>& right, const MPOTensor<Matrix, SymmGroup>& mpo)
{
    typename Boundary<Matrix, SymmGroup>::scalar_type res = 0.0;
    assert(left.aux_dim() == right.aux_dim());

    for(size_t i = 0; i < left.aux_dim(); i++)
    {
        // Exploit Hermiticity for the left boundary
        const block_matrix<Matrix, SymmGroup> && left_h = !mpo.herm_info.right_skip(i) ? static_cast<block_matrix<Matrix, SymmGroup> >(transpose(left[i])) : conjugate(left[mpo.herm_info.right_conj(i)]);
        // and for the right boundary
        const block_matrix<Matrix, SymmGroup> && right_h = !mpo.herm_info.left_skip(i) ? right[i] : adjoint(right[mpo.herm_info.left_conj(i)]);
        maquis::cout << " Left: " << i << std::endl;
        maquis::cout << left_h << std::endl;
        maquis::cout << " Right: " << i << std::endl;
        maquis::cout << right_h << std::endl;

        // assert(left_h.right_basis() == right_h.left_basis());
        if(left_h.right_basis() != right_h.left_basis()) continue;
        block_matrix<Matrix, SymmGroup> prod;
        gemm(left_h, right_h, prod);
        res += prod.trace();
        // it's probably cheaper to just multiply the matrices element-wise and add all the results

    }
    return res;
}
*/

#endif
