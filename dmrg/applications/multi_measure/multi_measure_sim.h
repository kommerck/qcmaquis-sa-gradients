/*****************************************************************************
 *
 * ALPS MPS DMRG Project
 *
 * Copyright (C) 2014 Institute for Theoretical Physics, ETH Zurich
 *               2011-2013 by Bela Bauer <bauerb@phys.ethz.ch>
 *                            Michele Dolfi <dolfim@phys.ethz.ch>
 *
 * This software is part of the ALPS Applications, published under the ALPS
 * Application License; you can use, redistribute it and/or modify it under
 * the terms of the license, either version 1 or (at your option) any later
 * version.
 *
 * You should have received a copy of the ALPS Application License along with
 * the ALPS Applications; see the file LICENSE.txt. If not, the license is also
 * available from http://alps.comp-phys.org/.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 *****************************************************************************/

#ifndef APP_DMRG_MEASURE_SIM_H
#define APP_DMRG_MEASURE_SIM_H

#include <cmath>
#include <iterator>
#include <iostream>
#include <sys/stat.h>

#include <boost/shared_ptr.hpp>

#include "dmrg/sim/sim.h"
#include "dmrg/optimize/optimize.h"

#include "dmrg/models/chem/measure_transform.hpp"

template <class Matrix, class SymmGroup>
class multi_measure_sim : public sim<Matrix, SymmGroup> {

    typedef sim<Matrix, SymmGroup> base;
    typedef optimizer_base<Matrix, SymmGroup, storage::disk> opt_base_t;

    using base::mps_sa;
    using base::mpo;
    using base::parms;
    using base::all_measurements;
    using base::stop_callback;
    using base::rfile_sa;

    using base::model;
    using base::fill_checkpoint_result_name;

public:

    multi_measure_sim (DmrgParameters & parms_)
    : base(parms_, parms_["n_states_sa"], fill_checkpoint_result_name(parms_, true))
    {
        if (!maquis::checks::sa_check(mps_sa, parms["lrparam_site"]))
            throw std::runtime_error("The specified checkpoints are not multistate-MPS, or lrparam_site is wrong.");
    }

    void run()
    {

        multi_measure();

        if (parms["MEASURE[Energy]"])
        {

            // Measure energy for all states
            // with multi-measurement
            MPS<Matrix, SymmGroup> & mps_b = mps_sa[0];
            int site = parms["lrparam_site"];

            MPO<Matrix, SymmGroup> mpoc = mpo;
            if (parms["use_compressed"])
                mpoc.compress(1e-12);

            // prepare boundaries
            std::pair<Boundary<Matrix, SymmGroup>, Boundary<Matrix, SymmGroup> > e_boundaries = prepare_boundaries(mps_sa[0], mps_sa[0], mpoc, site);

            for (int i = 0; i < mps_sa.size(); i++)
            {
                MPS<Matrix, SymmGroup>& mps = mps_sa[i];

                double energy;

                energy = maquis::real(expval_boundaries(mps, mps, mpoc, e_boundaries.first, e_boundaries.second, site)) + maquis::real(mpoc.getCoreEnergy());
                maquis::cout << "Energy -- state " << i << ": " << energy << std::endl;
                {
                    storage::archive ar(rfile_sa[i], "w");
                    ar["/spectrum/results/Energy/mean/value"] << std::vector<double>(1, energy);
                }
            }
        }

    }

private:

    void multi_measure()
    {
        typedef typename Model<Matrix, SymmGroup>::multi_measurements_type multi_measurements_type;

        multi_measurements_type meas = model.multi_measurements();

        for (auto&& m: meas)
            m->multi_evaluate_and_save(mps_sa, "/spectrum/results/", rfile_sa);

    }

};

#endif
